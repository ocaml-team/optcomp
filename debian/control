Source: optcomp
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper (>= 9),
 dh-ocaml,
 ocaml-findlib (>= 1.4),
 camlp4,
 ocamlbuild,
 ocaml
Standards-Version: 3.9.5
Section: ocaml
Homepage: https://github.com/diml/optcomp
Vcs-Browser: https://salsa.debian.org/ocaml-team/optcomp
Vcs-Git: https://salsa.debian.org/ocaml-team/optcomp.git

Package: optcomp
Architecture: any
Depends:
 liboptcomp-camlp4-dev,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: syntax extension for optional compilation with cpp-like directives (tools)
 Optcomp is a syntax extension which handles #if, #else, ... directives
 in OCaml source files. Compared to cpp:
  * it does not interpret //, /*, and */ as comment delimiters
  * it does not complains about missing '
  * it is easier to integrate in the build process when using other
    camlp4 syntax extensions
  * it does not do macro expansion while cpp does
 Compared to pa_macro, it does not require code that will be dropped to
 be valid OCaml code. This can be useful for code that optionnally uses
 GADTs, but can be compiled with older versions of OCaml.
 .
 This package contains command-line tools.

Package: liboptcomp-camlp4-dev
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: syntax extension for optional compilation with cpp-like directives (library)
 Optcomp is a syntax extension which handles #if, #else, ... directives
 in OCaml source files. Compared to cpp:
  * it does not interpret //, /*, and */ as comment delimiters
  * it does not complains about missing '
  * it is easier to integrate in the build process when using other
    camlp4 syntax extensions
  * it does not do macro expansion while cpp does
 Compared to pa_macro, it does not require code that will be dropped to
 be valid OCaml code. This can be useful for code that optionnally uses
 GADTs, but can be compiled with older versions of OCaml.
 .
 This package contains the syntax extension as a camlp4 module.
